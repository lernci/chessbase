# Create your views here.
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import render, redirect
from django.urls import reverse

# Create your views here.
from .models import Licenses, Schools, CEO, Teacher, Rating, Call, Called
from django.views import generic

from django.http import HttpResponseRedirect
from django.http import JsonResponse
import json
from django.db.models import Q


from django.core.mail import send_mail
from django.conf import settings

from syunikbase.forms import SubmitSchoolForm, SubmitTeacherForm, SubmitCallForm, EmailForm
def basehome(request):
	numshcools = Schools.objects.count()
	numteacher = Teacher.objects.count()

	context = {
		'num_shcools': numshcools,
		'numteacher': numteacher,
	}

	return render(request, 'index123.html', context=context)

class ShcoolsListView(generic.ListView):
    model = Schools
    context_object_name = 'my_list'
    template_name = 'shcools_list.html'  # Specify your own template name/location
    object_list = Schools.objects.all()
    #queryset = Schools.objects.all()

    def get_queryset(self):
        return Schools.objects.all() # Get 5 books containing the title war
        
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get the context
        context = super(ShcoolsListView, self).get_context_data(**kwargs)
        # Create any data and add it to the context
        context['some_data'] = 'This is just some data'
        context['form'] = SubmitSchoolForm()
      #  context['status_form'] = self.status_form

        return context

    def post(self, request, *args, **kwargs):
        print("Hello")
        form = SubmitSchoolForm(request.POST)
        if form.is_valid():
            #my_field = form.cleaned_data['my_field']
            # process the data as needed
            a = form.cleaned_data['comment']
            print(a)
            print("Valido")
            return redirect('schools')
        else:
            context = self.get_context_data(**kwargs)
            context['form'] = form
            #print(form.cleaned_data['comment'])

            return render(request, self.template_name, context)

class TeacherListView(generic.ListView):
    model = Teacher
    context_object_name = 'my_list'
    template_name = 'teacher_list.html'
    object_list = Teacher.objects.all()
    state = 2047

    def get_queryset(self):
        return Teacher.objects.all()

    def get_context_data(self, **kwargs):
        context = super(TeacherListView, self).get_context_data(*kwargs)
        context['form'] = SubmitTeacherForm()
        context['state'] = 2047
        return context

    def post(self, request, *args, **kwargs):
        print("Hello")
        form = SubmitTeacherForm(request.POST)
        
        if form.is_valid():
            #my_field = form.cleaned_data['my_field']
            # process the data as needed
            com = form.cleaned_data['comment']
            rt = form.cleaned_data['like']
            fname = form.cleaned_data['sname']
            #fname = 'Նարինե'
            techer = Teacher.objects.filter(id = fname)
            rate = Rating(teacher = techer[0], comment = com, teacher_rate = rt, problem = check)
            rate.save()
            check = form.cleaned_data['checkbox_field'] 
            print("asdfasdfaaaaaaaaaaa")
            print(check)

          #  print(a)
            print("Valido")
            print(self.state)
            context = self.get_context_data(**kwargs)
            context['state'] = form.cleaned_data['state']
            print(self.state)
            return render(request, self.template_name, context)

#            return redirect('teacher', context)
        else:
            context = self.get_context_data(**kwargs)
            context['form'] = form
            #print(form.cleaned_data['comment'])

            return render(request, self.template_name, context)

class CallListView(generic.ListView):
    model = Call
    context_object_name = 'call_list'
    def get_queryset(self):
        return Call.objects.all() # Get 5 books containing the title war
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get the context
        context = super(CallListView, self).get_context_data(**kwargs)
        # Create any data and add it to the context
        context['some_data'] = 'This is just some data'
        return context



class CallDetailView(generic.DetailView):
    model = Call
    template_name = 'syunikbase/call_detail.html'
    def call_detail_view(request, primary_key):
        try:
            call = Call.objects.get(pk = primary_key)

        except:
            raise Http404('Book does not exist')
        return render(request, 'syunikbase/call_detail.html', context={'called': called})

    def get_context_data(self, **kwargs):
        context = super(CallDetailView, self).get_context_data( **kwargs)
        context['form'] = SubmitCallForm()
        context['forme'] = EmailForm()
        context['teacher'] = self.get_object().get_teachers()
        context['called'] = self.get_object().get_called()
        return context

    def post(self, request, *args, **kwargs):
        print("Hello")
        #form = SubmitCallForm(request.POST)
        

        if 'savecomment' in request.POST:
            form = SubmitCallForm(request.POST)
            if form.is_valid():
                #my_field = form.cleaned_data['my_field']
                # process the data as needed
                com = form.cleaned_data['comment']
                rt = form.cleaned_data['like']
                fid = form.cleaned_data['sname']
                #fname = 'Նարինե'
                techer = Teacher.objects.filter(id = fid)
                check = form.cleaned_data['checkbox_field']
                if check:
                    pstatus = 'o'
                else:
                    pstatus = 'r' 
                rate = Rating(teacher = techer[0], comment = com, teacher_rate = rt, problem = check, problem_status = pstatus, call = self.get_object() )
                rate.save()
                check = form.cleaned_data['checkbox_field'] 
                print("asdfasdf")
                print(check)


              # print(a)
                print("Valido")
                link = str()
                recipe_id = self.get_object().id
                return HttpResponseRedirect(reverse('call-detail', kwargs={'pk': recipe_id}))
            else:
                print("Invalid")
                context = self.get_context_data(**kwargs)
                context['form'] = form
                return render(request, self.template_name, context)

            #print(form.cleaned_data['comment'])

        if 'savechanges' in request.POST:
            print ("Save Changes")
            recipe_id = self.get_object().id
            return HttpResponseRedirect(reverse('call-detail', kwargs={'pk': recipe_id}))
  
        return render(request, self.template_name, context)

def savechanges(request):
    print ("Worked Save Changes")
  #  print (pk)
    input_data = request.POST.get('data')
    my_data = json.loads(input_data)
    recipe_id = my_data[0]['page_id']

    data_size = len(my_data)
    for i in range(1, data_size):
        print(my_data[i]['ch'])
        c_called = Called.objects.get(pk = my_data[i]['id'])
        #c_teacher = Teacher.objects.get(pk = my_data[i]['id'])
        if my_data[i]['ch']:
            c_called.is_called = True
        else:
            c_called.is_called = False
        c_called.save()

    print(recipe_id)
    print(input_data)
    return HttpResponseRedirect(reverse('call-detail', kwargs={'pk': recipe_id }))

def getteacherhistory(request, pk):
    print("Get getteacherhistory")
    print(pk)
    t_history = Rating.objects.filter(teacher__id = pk)
    data = []
    for i in t_history:
        
        c_comment = {'date': i.created_date.strftime('%Y-%m-%d %H:%M:%S'), 'comment' : i.comment, 'rate': i.teacher_rate}
        js_comment = json.dumps(c_comment)
        
        data.append(c_comment)
#    js_data = json.dumps(data)
    return JsonResponse(data, safe=False)

def getteacherlistbyfilter(request, pk):
  

    a = pk
    g = a%2
    print("aaaaaaaa")
    print("a =" + str(a))
    a = a//2
    s = a%2
    print("a =" + str(a))
    a = a//2
    k = a%2
    print("a =" + str(a))
    a = a//2
    m = a%2
    print("a =" + str(a))
    a = a//2
    q = a%2
    
    a = a//2
    c = a%2

    a = a//2
    j = a%2

    a = a//2
    at = a%2

    a = a//2
    st = a%2

    a = a//2
    wk = a%2

    a = a//2
    r = a%2


#    print(s)
 #   print(k)
  #  print(m)
   # print(q)
    #print ("st=" + str(st))

    goris = Q(city="Invalid")
    sisian = Q(city = "Invalid")
    kapan = Q(city = "Invalid")
    meghri = Q(city = "Invalid")
    qajaran = Q(city = "Invalid")

    chuni = Q(license_status="Invalid")
    jamanakavor = Q(license_status="Invalid")
    atestat = Q(license_status="Invalid")
    stacac = Q(license_status = "Invalid") 

    worked = Q(license_status = "Invalid") 
    reserv = Q(license_status = "Invalid") 

    if (g == 1 ):
        goris = Q(city="Գ")

    if (s == 1):
        sisian = Q(city = "Ս")

    if (k == 1):
        kapan = Q(city = "Կ")
    if ( m == 1):
        meghri = Q(city = "Մ")
    if (q == 1):
        qajaran = Q(city = "Ք")

    if (c == 1):
        chuni = Q(license_status="Լիցենզիա չունի")

    if (j == 1):
        jamanakavor = Q(license_status="Ժամանակավոր")

    if (at == 1):
        atestat = Q(license_status="Ատեստավորված")

    if (st == 1):
        stacac = Q(license_status = "Ստացած")

    if (wk == 1):
        wk_ids = [techer.id for techer in Teacher.objects.all() if techer.getSchools() != "Ռեզերվային"]
        worked = Q(id__in = wk_ids)

    if (r == 1):
        wk_ids = [techer.id for techer in Teacher.objects.all() if techer.getSchools() == "Ռեզերվային"]
        reserv = Q(id__in = wk_ids)

  
    teacher_list = Teacher.objects.filter((goris | sisian | kapan | qajaran | meghri) & (chuni | jamanakavor | atestat | stacac) & (worked | reserv))

    data = []
    for i in teacher_list:
        c_comment = {
            'id': i.id,
            'first_name': i.first_name,
            'last_name' : i.last_name,
            'email' : i.email, 
            'phone_number': i.phone_number,
            'english': i.english,
            'city': i.city,
            'license_status': i.license_status,
            'license_days': i.license_days,
            'schools': i.getSchools()
        }
        js_comment = json.dumps(c_comment)
        data.append(c_comment)
    return JsonResponse(data, safe=False)

def updatelicense(request):
    print("Working update licnse")
    #email()

    teachers = Teacher.objects.all()
    for i in teachers:
        i.update_license()
    data = []
    return JsonResponse(data, safe=False)


def createcall(request):
    print("Create Call is Called")
    input_data = request.POST.get('data')
    my_data = json.loads(input_data)
    print(my_data)
    recipe_name = my_data[0]['name']
    state = my_data[1]['state']
    newcall = Call(name = recipe_name)
    newcall.save()
    data_size = len(my_data)
    for i in range(2, data_size):
        c_teacher = Teacher.objects.get(pk = my_data[i]['id'])
        called = Called(call = newcall, teacher = c_teacher, is_called=False)
        called.save()
    #recipe_id = my_data[0]['id']
    #return HttpResponseRedirect(reverse('teacher', context={'state': state }))
    return render(request, 'teacher_list.html', context={'state': state})

def email():
    subject = 'Thank you for registering to our site'
    message = ' it  means a world to us '
    email_from = settings.EMAIL_HOST_USER
    recipient_list = ['arturkhojabaghyan@gmail.com',]
    send_mail( subject, message, email_from, recipient_list )
    return 0

def sendemail(request):
    data = []
    if request.method == 'POST':
        form = EmailForm(request.POST)
        if form.is_valid():
            email_from = settings.EMAIL_HOST_USER
 #           to_email = form.cleaned_data['to_email']
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            page_id =  request.POST.get('page_id')
            calls = Call.objects.filter(id = page_id)
            for i in calls[0].get_teachers():
                send_mail(subject, message, email_from, [i.email])
                print (i.email)

            # Send the email using Django's send_mail function
            

            #return redirect('email_sent')  # Assuming you have a URL named 'email_sent'
    else:
        form = EmailForm()

    return HttpResponseRedirect(reverse('call-detail', kwargs={'pk': page_id }))

    #return render(request, 'email_form.html', {'form': form})