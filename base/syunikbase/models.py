from django.db import models

# Create your models here.

#class Schools(models.Model):
#	pass

import datetime
from dateutil.relativedelta import relativedelta

from django.core.validators import MaxValueValidator, MinValueValidator

from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.urls import reverse


class Teacher (models.Model):
    first_name = models.CharField(max_length=100,  help_text="Անուն")
    last_name = models.CharField(max_length=100)
    father_name = models.CharField(max_length=100, blank=True,)
    date_of_birth = models.DateField(null=True, blank=True)
    phone_number = models.CharField(max_length=100)
    email = models.EmailField(max_length=254, help_text="Ուսուցչի էլ․ հասցեն")

    education = models.CharField(max_length=100, blank=True,)
    prof = models.CharField(max_length=100,  help_text="Մասնագիտությունը", blank=True,)
    university = models.CharField(max_length=100,  help_text="Համալսարանը", blank=True,)
    uni_date = models.DateField(null=True, blank=True, )
    start_date_school = models.CharField(max_length=100,  help_text="Դպրոցում աշխատելու մեկնարկի ամսաթիվը", blank=True,)

    def set_status(self, st, d):
        self.license_status = st
        self.license_days = d
        print (st)
        self.save()

    CITY = (
        ('Գ', 'Գորիս'),
        ('Կ', 'Կապան'),
        ('Մ', 'Մեղրի'),
        ('Ք', 'Քաջարան'),
        ('Ս', 'Սիսիան'),
    )

    ENGLISH = (
    	('Գ', 'Գերազանց'),
    	('Լ', 'Լավ'),
    	('Բ', 'Բավարար'),
    	('Ա', 'Անբավարար'),
    )

    RUSSIAN = (
    	('Գ', 'Գերազանց'),
    	('Լ', 'Լավ'),
    	('Բ', 'Բավարար'),
    	('Ա', 'Անբավարար'),
    )

    english = models.CharField(
    	max_length = 1,
        choices=ENGLISH,
        blank=True,
        default='Ա',
    )

    russian = models.CharField(
    	max_length = 1,
        choices=RUSSIAN,
        blank=True,
        default='Ա',
    )
   

    city = models.CharField(
        max_length = 1,
        choices=CITY,
        blank=True,
        default='Գ',
    )

    license_status = models.CharField(max_length=100, default="Լիցենզիա չունի", null=True)
    license_days = models.IntegerField(default=-5000)

    def getSchools(self):
        shcools = Schools.objects.filter(teacher = self.id)
        names = ""
        for i in shcools:
            names += i.name
            names += ","
        if names == "":
            names = "Ռեզերվային"
        return names


    def __str__(self):
        return self.first_name + '  ' + self.last_name

    def update_license(self):
        print("Working update license" + self.last_name)
        li = Licenses.objects.filter(teacher = self)
        for lic in li[::-1]:
            enddate = lic.startdate + relativedelta(months=lic.month)
            days1 = (enddate - datetime.date.today()).days
            self.license_days = days1
            if days1 < 0:
                license_status = "Լիցենզիա չունի"
        self.save()


class CEO (models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=100)
    email = models.EmailField(max_length=254, help_text="Ուսուցչի էլ․ հասցեն")
    #school = models.ForeignKey(Schools,on_delete=models.CASCADE, help_text='Շախմատ առարկայի ուսուցիչը')
    other_details = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.first_name + '  ' + self.last_name

class Schools (models.Model):
    name = models.CharField(max_length=200, help_text='Դպրոցի անվանումը')
    teacher = models.ManyToManyField(Teacher, help_text='Շախմատ առարկայի ուսուցիչը', blank=True)
    ceo = models.OneToOneField(CEO, on_delete=models.SET_NULL, null=True, help_text='Շախմատ առարկայի ուսուցիչը')
    phone_number = models.CharField(max_length=100, null=True)
    email = models.EmailField(max_length=254, null=True,help_text="Ուսուցչի էլ․ հասցեն")
    address = models.CharField(max_length=200, null=True, help_text='Դպրոցի անվանումը')
    CITY = (
        ('Գ', 'Գորիս'),
        ('Կ', 'Կապան'),
        ('Մ', 'Մեղրի'),
        ('Ք', 'Քաջարան'),
        ('Ս', 'Սիսիան'),
    )

    city = models.CharField(
        max_length = 1,
        choices=CITY,
        blank=True,
        default='Գ',
    )

    def __str__(self):
        return self.name
	

class Licenses (models.Model):
        teacher = models.ForeignKey(Teacher, on_delete=models.SET_NULL, null=True)
        number = models.CharField(max_length=200, help_text='Վկայականի համարը')
        rate = models.IntegerField(validators=[MaxValueValidator(20), MinValueValidator(1)])
        month = models.IntegerField()
        startdate = models.DateField(null=True, blank=True)
        image = models.ImageField(upload_to='images', blank=True)
        email_sent = models.BooleanField(blank = True, default = False, help_text="Ուղղարկել նամակ ուսուցչի տվյալների մասին")

        TYPE = (
        	('Ժ', 'Ժամանակավոր'),
        	('Պ', 'Ատեստավորված'),
        	('Վ', 'Ստացած'),
        )
        license_type = models.CharField(
        max_length = 1,
        choices=TYPE,
        blank=True,
        default='Ժ',
    	)

        def __str__(self):
            return str(self.number)


        def save(self, *args, **kwargs):
            enddate = self.startdate + relativedelta(months=self.month)
            days1 = (enddate - datetime.date.today()).days
            self.teacher.set_status(self.get_license_type_display(), days1)

            super(Licenses, self).save(*args, **kwargs)


class Call(models.Model):
    name = models.CharField(max_length=200, help_text='Զանգի նպատակը')
    #teacher = models.ManyToManyField(Teacher, help_text='Շախմատ առարկայի ուսուցիչը')
    #teachers = Teacher.objects.all()
    def get_teachers(self):
        called = Called.objects.filter(call = self)
        teachers = Teacher.objects.none()
        for c in called:
            teachers = teachers | Teacher.objects.filter(id = c.teacher.id)
        print (teachers)
        return teachers

    def get_called(self):
        called = Called.objects.filter(call = self)
        return called

    def get_absolute_url(self):
        """Returns the URL to access a detail record for this book."""
        return reverse('call-detail', args=[str(self.id)])
    def __str__(self):
        return self.name

class Called(models.Model):
    call = models.ForeignKey(Call, on_delete=models.SET_NULL, null=True)
    teacher = models.ForeignKey(Teacher, on_delete=models.SET_NULL, null=True)
    is_called = models.BooleanField(default=False)



class Rating(models.Model):
    teacher = models.ForeignKey(Teacher, on_delete=models.SET_NULL, null=True)
    comment = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)


    RATE = (
        ('5', 'Գերազանց'),
        ('4', 'Լավ'),
        ('3','Բավարար'),
        ('2','Վատ'),
        ('1','Շատ վատ'),
        ('0','Նեյտրալ'),
    )

    STATUS = (
        ('o', 'Բաց'),
        ('i', 'Ընթացքում է'),
        ('r', 'Լուծված է'),
    )
    problem = models.BooleanField(default=False)

    teacher_rate = models.CharField(max_length = 1, choices = RATE, blank=True, default='0')
    problem_status = models.CharField(max_length = 1, choices = STATUS, blank=True, default='r')
    call = models.ForeignKey(Call, on_delete=models.SET_NULL, null=True)



@receiver(pre_delete, sender=Licenses)
def signal_function_name(sender, instance, using, **kwargs):
   # print(instance.teacher.last_name)
    if instance.teacher is None:
        return
    instance.teacher.set_status("Լիցենզիա չունի", -5000)
