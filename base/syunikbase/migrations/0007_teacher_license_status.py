# Generated by Django 3.2.16 on 2023-01-26 20:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('syunikbase', '0006_auto_20230126_2036'),
    ]

    operations = [
        migrations.AddField(
            model_name='teacher',
            name='license_status',
            field=models.BooleanField(default=False),
        ),
    ]
