# Generated by Django 3.2.12 on 2023-08-21 10:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('syunikbase', '0021_auto_20230821_1005'),
    ]

    operations = [
        migrations.AddField(
            model_name='ceo',
            name='address',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='ceo',
            name='other_details',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
