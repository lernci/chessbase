from django.urls import path
from . import views

urlpatterns = [
    path('schools', views.ShcoolsListView.as_view(),name='schools'),
    path('teacher', views.TeacherListView.as_view(),name='teacher'),
    path('', views.basehome, name='basehome'),
    path('calls/', views.CallListView.as_view(), name='calls'),
    path('calls/<int:pk>', views.CallDetailView.as_view(), name='call-detail'),
    path('savechanges', views.savechanges, name='savechanges'),
    path('getteacherhistory/<int:pk>', views.getteacherhistory, name='getteacherhistory'),
    path('getteacherlistbyfilter/<int:pk>', views.getteacherlistbyfilter, name='getteacherlistbyfilter'),
    path('updatelicense', views.updatelicense, name='updatelicense'),
    path('sendemail', views.sendemail, name='sendemail'),
    path('createcall', views.createcall, name='createcall'),

]