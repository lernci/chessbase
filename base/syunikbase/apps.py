from django.apps import AppConfig


class SyunikbaseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'syunikbase'
