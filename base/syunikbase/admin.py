from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from django.db import models
from django.forms import CheckboxSelectMultiple


from .models import Schools, Teacher, CEO, Licenses, Rating, Call, Called
import datetime
from dateutil.relativedelta import relativedelta

from django.core.mail import send_mail, EmailMessage
from django.conf import settings
import os

#admin.site.register(Schools)
#admin.site.register(Teacher)
admin.site.register(CEO)
#admin.site.register(Licenses)
class TeacherInline (admin.TabularInline):
    model = Teacher

@admin.register(Schools)
class SchoolsAdmin(admin.ModelAdmin):
    
    list_display = ('name', 'city','link_to_ceo', 'get_teacher', 'phone_number', 'email')
    list_filter = ('city',)
    def link_to_ceo(self, obj):
        link = reverse("admin:syunikbase_ceo_change", args=[obj.ceo.id])
        text = obj.ceo.last_name + ' ' + obj.ceo.first_name
        return format_html('<a href="{}">{}</a>', link, text)

    def get_teacher(self, obj):
        rt = ""
        for tch in obj.teacher.all():
            lt = reverse ("admin:syunikbase_teacher_change", args=[tch.id])
            ltext = tch.last_name + ' ' + tch.first_name
            col = "green"
            if tch.license_days < 0:
            	col = "red"
            lhtml = format_html('<a style="color: {}" href="{}">{}</a>', col, lt, ltext)
            rt +=  lhtml + '<br/>'
        return format_html(rt)
    link_to_ceo.short_description = 'Տնօրեն'
    get_teacher.short_description = 'Շախմատի Ուսուցիչ'

# Register your models here.

@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ( 'last_name', 'first_name', 'city','phone_number', 'email', 'get_schools', 'get_license', 'english', 'russian')

    def get_schools(self, obj):
        shcools = Schools.objects.filter(teacher = obj.id)
        sht = ""
        for s in shcools:
            ls = reverse("admin:syunikbase_schools_change", args=[s.id])
            lt = s.name
            htmls = format_html('<a href="{}">{}</a>', ls, lt)
            sht += htmls + '<br/>'
        if not shcools:
        	sht = format_html ('<p style="color: red"> Ազատ է </p>')
        return format_html(sht)

    def get_license(self, obj):
    	li = Licenses.objects.filter(teacher = obj.id)
    	rt = ""
    	for lic in li[::-1]:
    		liclink = reverse("admin:syunikbase_licenses_change", args=[lic.id])
    		enddate = lic.startdate + relativedelta(months=lic.month)
    		days1 = (enddate - datetime.date.today()).days
    		ltext = str(days1) + ',  ' + str(obj.license_status)
    		if days1 < 0:
    			col = "red"
    		if days1 > 0:
    			col = "green"
    		rt += format_html('<a style="color: {}" href="{}">{}</a>', col, liclink, ltext) + '<br/>'
    	if not li:
    		rt = format_html('<p style="color: red"> Լիցենզիա չունի </p>')
    	return format_html(rt)


@admin.register(Licenses)
class LicensesAdmin(admin.ModelAdmin):
    list_display = ('number', 'get_teacher', 'rate', 'month', 'startdate',)

    def get_teacher(self, obj):
        if obj.teacher is None:
            return "Default"
        link = reverse("admin:syunikbase_teacher_change", args=[obj.teacher.id])
        text = obj.teacher.last_name + ' ' + obj.teacher.first_name
        return format_html('<a href="{}">{}</a>', link, text)

    def save_model(self, request, obj, form, change):
        print("Saved Licenses")
        super().save_model(request, obj, form, change)
        if obj.email_sent:
            self.send_email_with_attachment(obj)

    def send_email_with_attachment(self, obj):

        emailto = obj.teacher.email
        email_from = settings.EMAIL_HOST_USER
        subj = "Շախմատի ուսուցչի լիցենզիա | " + obj.teacher.first_name+ " "  + obj.teacher.last_name 
        body = "Հարգելի "+obj.teacher.first_name+" "+obj.teacher.last_name +"\n"
        body += "«Շախմատ» առարկայի ձեր վերապատրաստումից հետո կայացած քննության արդյունքում դուք ստացել ենք " + str(obj.rate) + " միավոր եվ "
        body += obj.startdate.strftime("%Y-%m-%d") + "-ի դրությամբ կարող եք դասավանդել " + str(obj.month/12) + " տարի։\n"
        body += "Նամակին կցված է Ձեր լիցենզավորման վկայականը։ \n\nՀարցերի դեպքում կարող եք զանգահարել 055 255 256:\n"
        body+= "Հարգանքով՝ Արտուր Խոջաբաղյան"
        schools = Schools.objects.filter(teacher = obj.teacher.id)
        ccmail = []
        for s in schools:
            ccmail.append(s.email)
        ccmail.append("arturkhojabaghyan@gmail.com")


        email = EmailMessage(
            subj,
            body,  # Plain text version of the email
            email_from,
            [emailto],
            cc=ccmail
        )

        # Attach the file to the email
        print(obj.image)
        image_path = obj.image.path # Replace with the actual path to your file
        with open(image_path, 'rb') as file:
            email.attach(os.path.basename(image_path), file.read(), 'text/plain')  # Adjust the MIME type as needed

        # Attach the HTML version of the email
        #email.attach_alternative(html_content, 'text/html')

        # Send the email
        email.send() 

@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
    list_display = ('teacher', 'comment', 'teacher_rate', 'problem_status')


@admin.register(Call)
class CallAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }
    list_display = ('name', 'get_teachers')

@admin.register(Called)
class CalledAdmin(admin.ModelAdmin):
    list_display = ('call', 'teacher', 'is_called',)