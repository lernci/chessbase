import datetime

from django import forms
from .models import Teacher

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

class SubmitSchoolForm(forms.Form):
	comment = forms.CharField(help_text="Thisi is a test field")	
	sname = forms.CharField(help_text="Thisi is a test field")

class SubmitTeacherForm(forms.Form):
    comment = forms.CharField(help_text="Thisi is a test field", widget=forms.Textarea)	
    sname = forms.CharField(help_text="Thisi is a test field")
    state = forms.IntegerField(help_text="Thisi is a state field")

    RATE = [
        ('5', 'Գերազանց'),
        ('4', 'Լավ'),
        ('3','Բավարար'),
        ('2','Վատ'),
        ('1','Շատ վատ'),
        ('0','Նեյտրալ'),
    ]

    checkbox_field = forms.BooleanField(
        required=False,
        initial=False,
    )

    like = forms.CharField(label = 'Rate this teacher', widget=forms.RadioSelect(choices=RATE))

class SubmitCallForm(forms.Form):
    comment = forms.CharField(help_text="Thisi is a test field", widget=forms.Textarea) 
    sname = forms.CharField(help_text="Thisi is a test field")
    #t = forms.ModelMultipleChoiceField(queryset=Teacher.objects.all(), widget=forms.CheckboxSelectMultiple)
    #a = forms.CharField(help_text="Thisi is a test field", widget=forms.Textarea) 

    RATE = [
        ('5', 'Գերազանց'),
        ('4', 'Լավ'),
        ('3','Բավարար'),
        ('2','Վատ'),
        ('1','Շատ վատ'),
        ('0','Նեյտրալ'),
    ]

    checkbox_field = forms.BooleanField(
        required=False,
        initial=False,
    )

    like = forms.CharField(label = 'Rate this teacher', widget=forms.RadioSelect(choices=RATE))

class myujform(forms.Form):
    t = forms.ModelMultipleChoiceField(queryset=Teacher.objects.all(), widget=forms.CheckboxSelectMultiple)

class EmailForm(forms.Form):
#    to_email = forms.EmailField(label='To', required=True)
    subject = forms.CharField(label='Subject', max_length=100, required=True)
    message = forms.CharField(label='Message', widget=forms.Textarea, required=True)